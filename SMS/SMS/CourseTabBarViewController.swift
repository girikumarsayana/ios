//
//  CourseTabBarViewController.swift
//  SMS
//
//  Created by Giri Sayana,Satyanarayana Pavuluri on 5/4/16.
//  Copyright © 2016 Giri Sayana,Satyanarayana Pavuluri. All rights reserved.
//

import UIKit

class CourseTabBarViewController: UITabBarController {
    var course : Course!
    
    weak var vcReference : ViewController!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
//        print("Course \(course)")
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Take Attendance", style: .Plain, target: self, action: Selector("takeAttendance:"))
        
    }

    
    func takeAttendance(sender:UIBarButtonItem!){
        let attendanceVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("AttendanceVC") as! AttendanceVC
        attendanceVC.currentCourse = course
        attendanceVC.vcReference = self.vcReference        
        self.presentViewController(attendanceVC, animated: true) {}

    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
