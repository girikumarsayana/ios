//
//  CourseCell.swift
//  SMS
//
//  Created by Giri Sayana,Satyanarayana Pavuluri on 4/26/16.
//  Copyright © 2016 Giri Sayana,Satyanarayana Pavuluri. All rights reserved.
//

import UIKit

class CourseCell: UITableViewCell {

    @IBOutlet weak var courseName: UILabel!
    
    @IBOutlet weak var deptName: UILabel!
    
    @IBOutlet weak var courseID: UILabel!
    
    
    func configureCell(course: Course){
        courseName.text = course.courseName
        deptName.text = course.departmentName
        courseID.text = course.courseId
    }
    
}
