//
//  Attendance.swift
//  SMS
//
//  Created by Giri Sayana,Satyanarayana Pavuluri on 4/26/16.
//  Copyright © 2016 Giri Sayana,Satyanarayana Pavuluri. All rights reserved.
//

import UIKit

class Attendance:NSObject {
    
    var attendanceDate : String!
    var attendanceDict : Dictionary<String, Bool>!
    
//    var students : [Student]!
    
    init(studentIds : [String]){
        attendanceDate = String()
        attendanceDict = Dictionary<String, Bool>()
        
        for s in studentIds{
            attendanceDict[s] = true
        }
    }
    
    func setAttendance(studentId : String, isPresent : Bool){
        attendanceDict[studentId] = isPresent
    }
    
    func getAttendance(studentId : String) -> Bool{
        return attendanceDict[studentId]!
    }
    
    
    func encodeWithCoder(aCoder: NSCoder!) {
        aCoder.encodeObject(attendanceDate, forKey:"attendanceDate")
        aCoder.encodeObject(attendanceDict, forKey:"attendanceDict")
    }
    
    
    init (coder aDecoder: NSCoder!) {
        self.attendanceDate = aDecoder.decodeObjectForKey("attendanceDate") as! String
        self.attendanceDict = aDecoder.decodeObjectForKey("attendanceDict") as! Dictionary<String, Bool>
    }
    
    
}
