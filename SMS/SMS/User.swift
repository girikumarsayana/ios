//
//  User.swift
//  SMS
//
//  Created by Giri Sayana,Satyanarayana Pavuluri on 5/5/16.
//  Copyright © 2016 Giri Sayana,Satyanarayana Pavuluri. All rights reserved.
//

import UIKit

class User : NSObject, NSCoding {
    var firstName : String!
    var lastName : String!
    var emailId : String!
    
    init(firstName : String, lastName : String , emailId : String){
        self.firstName = firstName
        self.lastName = lastName
        self.emailId = emailId
    }
    override init() {
        
    }
    required convenience init?(coder aDecoder: NSCoder) {
        self.init()
        self.firstName =  aDecoder.decodeObjectForKey("firstName") as! String
        self.lastName = aDecoder.decodeObjectForKey("lastName") as! String
        self.emailId = aDecoder.decodeObjectForKey("emailId") as! String
    }
    func encodeWithCoder(aCoder: NSCoder) {
        aCoder.encodeObject(self.firstName, forKey: "firstName")
        aCoder.encodeObject(self.lastName, forKey: "lastName")
        aCoder.encodeObject(self.emailId, forKey: "emailId")
    }
}
