//
//  ViewController.swift
//  SMS
//
//  Created by Giri Sayana,Satyanarayana Pavuluri on 4/25/16.
//  Copyright © 2016 Giri Sayana,Satyanarayana Pavuluri. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate{

    var courses = [Course]()
    
    @IBOutlet weak var imageButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var userImage: UIImageView!
    
    @IBOutlet weak var userName: UILabel!
    
    var imagePicker : UIImagePickerController!
    
    @IBOutlet weak var userEmailId: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        tableView.delegate = self
        tableView.dataSource = self
        
        imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        
        let firstName = NSUserDefaults.standardUserDefaults().objectForKey("userFirstName") as? String
        let lastName = NSUserDefaults.standardUserDefaults().objectForKey("userLastName") as? String
        userName.text = lastName! + " " + firstName!
        userEmailId.text = NSUserDefaults.standardUserDefaults().objectForKey("userEmail") as? String
        
        if let imageData = NSUserDefaults.standardUserDefaults().objectForKey("userImage"), let image = UIImage(data: imageData as! NSData){
                userImage.image = image
                imageButton.setTitle("", forState: .Normal)
        }
        
        fillCoursesFromFile()
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage, editingInfo: [String : AnyObject]?) {
        imagePicker.dismissViewControllerAnimated(true, completion: nil)
        userImage.image = image
        NSUserDefaults.standardUserDefaults().setObject(UIImagePNGRepresentation(image), forKey: "userImage")
    }
    
    @IBAction func changeUserImage(sender: UIButton) {
        sender.setTitle("", forState: .Normal)
        presentViewController(imagePicker, animated: true, completion: nil)       
    }
    
    func storeToFile(){
        let paths = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory,NSSearchPathDomainMask.AllDomainsMask, true)
        let path: AnyObject = paths[0]
        let arrPath = path.stringByAppendingString("/array.plist")
        NSKeyedArchiver.archiveRootObject(courses, toFile: arrPath)
    }
//
//    func saveImageAndCreatePath(image : UIImage) -> String{
//        let imgData = UIImagePNGRepresentation(image)
//        let imgPath =  "userImage.png"
//        let fullPath = documentPathForFileName(imgPath)
//        imgData?.writeToFile(fullPath, atomically: true)
//        return imgPath
//        
//    }
//    
//    func imageForPath(path: String) -> UIImage? {
//        let fullPath = documentPathForFileName(path)
//        let image = UIImage(named: fullPath)
//        return image
//    }
//    
//    func documentPathForFileName(name: String) -> String{
//        let paths = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)
//        let fullPath = paths[0] as NSString
//        return fullPath.stringByAppendingPathComponent(name)
//    }
    
    func fillCoursesFromFile(){
        let paths = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory,NSSearchPathDomainMask.AllDomainsMask, true)
        let path: AnyObject = paths[0]
        let arrPath = path.stringByAppendingString("/array.plist")

        if let tempArr: [Course] = NSKeyedUnarchiver.unarchiveObjectWithFile(arrPath) as? [Course] {
            courses = tempArr
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        self.tableView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 69
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return courses.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {

        let courseCell = tableView.dequeueReusableCellWithIdentifier("CourseCell") as! CourseCell
        courseCell.configureCell(courses[indexPath.row])
        courseCell.accessoryType = .DisclosureIndicator
        return courseCell

    }
    
    func colorForIndex(index: Int) -> UIColor
    {
        let itemCount = courses.count - 1
        let color = (CGFloat(index) / CGFloat(itemCount)) * 0.5
        return UIColor(red: color, green: 0.6, blue: 0.10, alpha: 1.0)
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath)
    {
        if (indexPath.row % 2 == 0)
        {
            cell.backgroundColor = colorForIndex(indexPath.row)
        } else {
            cell.backgroundColor = UIColor.whiteColor()
        }
    }
    
    func onCourseAdded(notif : AnyObject){
        tableView.reloadData()
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "addCourse" {
            let acVC = segue.destinationViewController as! AddCourseVC
            acVC.coursesVC = self
        }else if segue.identifier == "viewCourseStudents" {
            if tableView.indexPathForSelectedRow != nil {
                
                let barViewControllers = segue.destinationViewController as! CourseTabBarViewController
                barViewControllers.course = courses[(tableView.indexPathForSelectedRow?.row)!]
                barViewControllers.vcReference = self
                let studentsVC = barViewControllers.viewControllers![0] as! ViewCourseStudentsVC
                studentsVC.course = courses[(tableView.indexPathForSelectedRow?.row)!]
                studentsVC.vcReference = self
                
                let datesVC = barViewControllers.viewControllers![1] as! ViewCourseAttendanceTVC
                datesVC.course = courses[(tableView.indexPathForSelectedRow?.row)!]
                
            }
        }
    }
}

