//
//  StudentCell.swift
//  SMS
//
//  Created by Giri Sayana,Satyanarayana Pavuluri on 4/30/16.
//  Copyright © 2016 Giri Sayana,Satyanarayana Pavuluri. All rights reserved.
//

import UIKit

class StudentCell: UITableViewCell {
    
    @IBOutlet weak var studentName: UILabel!
    
    @IBOutlet weak var emailId: UILabel!
    
    @IBOutlet weak var attendanceLabel: UILabel!
    
    func configureCell(student : Student){
        studentName.text =  "\(student.lastName) \(student.firstName)"
        emailId.text = student.emailId
        
        attendanceLabel.text = String(student.numberOfClassesAttended)
        
    }
    
}