//
//  AttendanceCell.swift
//  SMS
//
//  Created by Giri Sayana,Satyanarayana Pavuluri on 5/3/16.
//  Copyright © 2016 Giri Sayana,Satyanarayana Pavuluri. All rights reserved.
//

import UIKit

protocol SwitchProtocol {
    func isPrsentSwitchTapped(isPresent: Bool, studentId : String)
}


class AttendanceCell: UITableViewCell {
    
    var delegate : SwitchProtocol!

    @IBOutlet weak var studentName: UILabel!
    
    @IBOutlet weak var isPresentSwitch: UISwitch!
    
    var student: Student!
    
    func configureCell(){
        studentName.text =  "\(student.lastName) \(student.firstName)"
        isPresentSwitch.on = true
    }
    
    
    @IBAction func isPrsentSwitchTapped(sender: AnyObject) {
        
        if let x = delegate{
            x.isPrsentSwitchTapped(isPresentSwitch.on, studentId: student.studentId)
        }
    }
}
