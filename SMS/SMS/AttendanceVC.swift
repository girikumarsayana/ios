//
//  AttendanceVC.swift
//  SMS
//
//  Created by Giri Sayana,Satyanarayana Pavuluri on 5/3/16.
//  Copyright © 2016 Giri Sayana,Satyanarayana Pavuluri. All rights reserved.
//

import UIKit

class AttendanceVC: UIViewController,UITableViewDataSource, UITableViewDelegate, SwitchProtocol{

    @IBOutlet weak var attendanceTableView: UITableView!
    
    weak var vcReference : ViewController!
    
//    var studentsList : [Student]!
    
    var currentCourse : Course!
    
    var attendanceDict = Dictionary<String,Bool>()
    
    var studentIds : [String]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        studentIds = [String]()
        
        for student in currentCourse.students{
            let x = student.studentId
            attendanceDict[x] = true
            studentIds.append(student.studentId)
        }
        
    }
    
    func isPrsentSwitchTapped(isPresent: Bool, studentId : String){
        attendanceDict[studentId] = isPresent
    }
    
    
    @IBAction func SaveAttendancePressed(sender: AnyObject) {
        
        for k in attendanceDict.keys{
            print("\(k) - \(attendanceDict[k])")
        }
        // Create an Attendance Object.
        // Assign today's date to the date
        // Assign attendanceDict to the dict
        // Append this Attendance Objct to the [Attendance] list in the Course Object
        
        let att = Attendance(studentIds: studentIds)
        //att.attendanceDate = NSDate()
        
        
        let dateFormat = NSDateFormatter()
        let currentDate = NSDate()
        dateFormat.locale = NSLocale(localeIdentifier: "en_US")
//        dateFormat.dateStyle = NSDateFormatterStyle.LongStyle
        dateFormat.dateFormat = "EEEE, MMMM dd, yyyy, HH:mm"
        let convertedDate = dateFormat.stringFromDate(currentDate)
        
        
        att.attendanceDate = convertedDate
        att.attendanceDict = attendanceDict
        currentCourse.fullCourseAttendanceList.append(att)
        
        for k in att.attendanceDict.keys{
            for s in currentCourse.students{
                if k == s.studentId{
                    if att.attendanceDict[k] == true{
                        s.numberOfClassesAttended += 1
                    }
                }
            }
        }
        
        if let x = vcReference{
            x.storeToFile()
        }
        
        self.dismissViewControllerAnimated(true) {}
    }
    
    
    @IBAction func cancelPressed(sender: AnyObject) {
        self.dismissViewControllerAnimated(true) {}
    }
    
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return currentCourse.students.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if let attendanceCell = tableView.dequeueReusableCellWithIdentifier("AttendanceCell") as? AttendanceCell {
            
//            attendanceCell.studentName.text = studentsList[indexPath.row].firstName
//            attendanceCell.isPresentSwitch.on = true
            let student = currentCourse.students[indexPath.row]
            attendanceCell.student = student
            attendanceCell.configureCell()
            attendanceCell.delegate = self
            
            return attendanceCell
            
        }
        else{
            return AttendanceCell()
        }
    }

    
    
}
