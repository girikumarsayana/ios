//
//  RegisterUserVC.swift
//  SMS
//
//  Created by Giri Sayana,Satyanarayana Pavuluri on 5/5/16.
//  Copyright © 2016 Giri Sayana,Satyanarayana Pavuluri. All rights reserved.
//

import UIKit

class RegisterUserVC: UIViewController {

    
    let userDefaults = NSUserDefaults.standardUserDefaults()

    @IBOutlet weak var firstNameField: UITextField!
    
    @IBOutlet weak var lastNameField: UITextField!
    
    @IBOutlet weak var emailIdField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillLayoutSubviews() {
        if (userDefaults.objectForKey("userFirstName") != nil)  {
            let navVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("mainNav") as! UINavigationController
            self.presentViewController(navVC, animated: true) {}
        }
    }

    @IBAction func registerUser(sender: AnyObject) {
        let firstName = firstNameField.text!
        let lastName = lastNameField.text!
        let emailId = emailIdField.text!
        
        if(firstName.isEmpty || lastName.isEmpty || emailId.isEmpty){
            let myAlert = UIAlertController(title: "Alert", message: "All fields are required to fill in", preferredStyle: UIAlertControllerStyle.Alert)
            let okAction = UIAlertAction(title: "OK", style: .Default, handler: nil)
            myAlert.addAction(okAction)
            self.presentViewController(myAlert, animated: true, completion: nil)
            return
        }
        else{
            userDefaults.setValue(firstName, forKey: "userFirstName")
            userDefaults.setValue(lastName, forKey: "userLastName")
            userDefaults.setValue(emailId, forKey: "userEmail")
            let navVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("mainNav") as! UINavigationController
            self.presentViewController(navVC, animated: true) {}
        }
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
