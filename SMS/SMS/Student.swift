//
//  Student.swift
//  SMS
//
//  Created by Giri Sayana,Satyanarayana Pavuluri on 4/26/16.
//  Copyright © 2016 Giri Sayana,Satyanarayana Pavuluri. All rights reserved.
//

import UIKit

class Student:NSObject {
    var firstName : String!
    var lastName : String!
    var emailId : String!
    var studentId : String!
//    var fullName : String!
    
    var numberOfClassesAttended = 0
    
    init(firstName: String,lastName: String, emailId : String, studentId : String){
        self.studentId = studentId
        self.firstName = firstName
        self.lastName =  lastName
        self.emailId = emailId
        
    }
    
    func encodeWithCoder(aCoder: NSCoder!) {
        aCoder.encodeObject(firstName, forKey:"firstName")
        aCoder.encodeObject(lastName, forKey:"lastName")
        aCoder.encodeObject(emailId, forKey:"emailId")
        
        aCoder.encodeObject(studentId, forKey: "studentId")
//        aCoder.encodeObject(fullName, forKey: "fullName")
        aCoder.encodeObject(numberOfClassesAttended, forKey: "numClassesAttended")
    }
    
    
    init (coder aDecoder: NSCoder!) {
        self.firstName = aDecoder.decodeObjectForKey("firstName") as! String
        self.lastName = aDecoder.decodeObjectForKey("lastName") as! String
        self.emailId = aDecoder.decodeObjectForKey("emailId") as! String
        
        self.studentId = aDecoder.decodeObjectForKey("studentId") as! String
//        self.fullName = aDecoder.decodeObjectForKey("fullName") as! String
        self.numberOfClassesAttended = aDecoder.decodeObjectForKey("numClassesAttended") as! Int
    }
    
}
