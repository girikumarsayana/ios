//
//  viewCourseStudentsVC.swift
//  SMS
//
//  Created by Giri Sayana,Satyanarayana Pavuluri on 4/30/16.
//  Copyright © 2016 Giri Sayana,Satyanarayana Pavuluri. All rights reserved.
//

import UIKit

class ViewCourseStudentsVC: UIViewController {
    
    var course : Course!
    
    weak var vcReference : ViewController!
    
    @IBOutlet weak var studentsTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Take Attendance", style: .Plain, target: self, action: #selector(ViewCourseStudentsVC.takeAttendance(_:)))
        
        //self.navigationItem.title = "ASDFGHJKL"
    }
    
    override func viewDidAppear(animated: Bool) {
        self.studentsTableView.reloadData()
    }

    
    @IBAction func addCourseStudent(sender: AnyObject) {
        let addStudentVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("AddStudentVC") as! AddStudentVC
        addStudentVC.currentCourse = course
        addStudentVC.vcReference = self.vcReference
        self.presentViewController(addStudentVC, animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath)
    {
        if (indexPath.row % 2 == 0)
        {
            cell.backgroundColor = UIColor.grayColor()
        }
        else {
            cell.backgroundColor = UIColor.whiteColor()
        }
    }
   
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return course.students.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if let studentCell = tableView.dequeueReusableCellWithIdentifier("StudentCell") as? StudentCell {
            let courseStudent : Student = course.students[indexPath.row]
            studentCell.configureCell(courseStudent)            
            return studentCell
            
        }
        else{
            return StudentCell()
        }
    }

}
