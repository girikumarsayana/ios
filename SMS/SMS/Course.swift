//
//  Course.swift
//  SMS
//
//  Created by Giri Sayana,Satyanarayana Pavuluri on 4/26/16.
//  Copyright © 2016 Giri Sayana,Satyanarayana Pavuluri. All rights reserved.
//

import UIKit

class Course : NSObject {
    var courseId = String()
    var courseName = String()
    var departmentName = String()
    
    var students = [Student]()
    
//    var startDate : NSDate!
//    var endDate : NSDate!
    
    var fullCourseAttendanceList = [Attendance]()
    
    
    init(courseName: String, courseId : String, departmentName : String){
        self.courseId = courseId
        self.courseName = courseName
        self.departmentName = departmentName
    }
    
    func addStudent(student : Student){
        self.students.append(student)
    }
    
    func getNewAttendance() -> Attendance {
        var studentIds = [String]()
        for student in students{
            studentIds.append(student.studentId)
        }
        return Attendance(studentIds: studentIds)
    }
    
    func encodeWithCoder(aCoder: NSCoder!) {
        aCoder.encodeObject(courseId, forKey:"courseId")
        aCoder.encodeObject(courseName, forKey:"courseName")
        aCoder.encodeObject(departmentName, forKey:"departmentName")
        
        aCoder.encodeObject(students, forKey: "students")
        aCoder.encodeObject(fullCourseAttendanceList, forKey: "fullAttendance")
    }
    
    
    init (coder aDecoder: NSCoder!) {
        self.courseId = aDecoder.decodeObjectForKey("courseId") as! String
        self.courseName = aDecoder.decodeObjectForKey("courseName") as! String
        self.departmentName = aDecoder.decodeObjectForKey("departmentName") as! String
        
        self.students = aDecoder.decodeObjectForKey("students") as! [Student]
        self.fullCourseAttendanceList = aDecoder.decodeObjectForKey("fullAttendance") as! [Attendance]
    }
    
    
    
}
