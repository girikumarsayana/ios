//
//  AddPostVC.swift
//  SMS
//
//  Created by Giri Sayana,Satyanarayana Pavuluri on 4/25/16.
//  Copyright © 2016 Giri Sayana,Satyanarayana Pavuluri. All rights reserved.
//

import UIKit

class AddCourseVC: UIViewController {
    
    @IBOutlet weak var courseNameField: UITextField!
    
    @IBOutlet weak var courseIDField: UITextField!
    
    @IBOutlet weak var departmentField: UITextField!
    
    

    weak var coursesVC : ViewController!
    
    
    override func viewDidLoad() {
        super.viewDidLoad();
        
//        let newCourse = Course(courseName: courseNameField.text!, courseId: courseIDField.text!, departmentName: departmentField.text!)        
//        let aStudent = Student(firstName: "AAA", lastName:"BBB", emailId: "asdfgh", studentId: "1234")
//        newCourse.addStudent(aStudent)
//        coursesVC.courses.append(newCourse)        
    }
    
    @IBAction func addCourse(sender: AnyObject) {
        let courseName = courseNameField.text!
        let courseId = courseIDField.text!
        let dept = departmentField.text!
        
        if (courseName.isEmpty || courseId.isEmpty || dept.isEmpty) {
            let myAlert = UIAlertController(title: "Alert", message: "All fields are required to fill in", preferredStyle: UIAlertControllerStyle.Alert)
            let okAction = UIAlertAction(title: "OK", style: .Default, handler: nil)
            myAlert.addAction(okAction)
            self.presentViewController(myAlert, animated: true, completion: nil)
            return
        }
        else{
            let newCourse = Course(courseName: courseNameField.text!, courseId: courseIDField.text!, departmentName: departmentField.text!)
//            let aStudent = Student(firstName: "AAA", lastName:"BBB", emailId: "asdfgh", studentId: "1234")
//            newCourse.addStudent(aStudent)
            coursesVC.courses.append(newCourse)
            coursesVC.storeToFile()
            dismissViewControllerAnimated(true, completion: nil)
        }

    }
    
    @IBAction func cancelAddCourse(sender: AnyObject) {
        dismissViewControllerAnimated(true, completion: nil)
    }
}
