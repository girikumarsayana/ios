//
//  AddStudentVC.swift
//  SMS
//
//  Created by Giri Sayana,Satyanarayana Pavuluri on 4/30/16.
//  Copyright © 2016 Giri Sayana,Satyanarayana Pavuluri. All rights reserved.
//

import UIKit

class AddStudentVC: UIViewController {
    
    var currentCourse : Course!
    
    @IBOutlet weak var firstNameField: UITextField!
    
    @IBOutlet weak var lastNameField: UITextField!
    
    @IBOutlet weak var emailIdField: UITextField!
    
    @IBOutlet weak var studentIdField: UITextField!
    
    weak var vcReference : ViewController!
    
    
    override func viewDidLoad() {
        super.viewDidLoad();
    }
    
    @IBAction func addStudent(sender: AnyObject) {
        let firstName = firstNameField.text!
        let lastName = lastNameField.text!
        let emailId = emailIdField.text!
        let studentId = studentIdField.text!
        
        if(firstName.isEmpty || lastName.isEmpty || emailId.isEmpty || studentId.isEmpty){
            let myAlert = UIAlertController(title: "Alert", message: "All fields are required to fill in", preferredStyle: UIAlertControllerStyle.Alert)
            let okAction = UIAlertAction(title: "OK", style: .Default, handler: nil)
            myAlert.addAction(okAction)
            self.presentViewController(myAlert, animated: true, completion: nil)
            return
        }
        else{
            let aStudent = Student(firstName: firstName, lastName: lastName, emailId: emailId, studentId: studentId)
            currentCourse.students.append(aStudent)
        
            if let x = vcReference{
                x.storeToFile()
            }
            self.dismissViewControllerAnimated(true, completion: nil)
        }
    }
    
    @IBAction func cancelAddStudent(sender: AnyObject) {
        dismissViewControllerAnimated(true, completion: nil)
    }
}
